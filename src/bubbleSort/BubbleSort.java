package bubbleSort;


/**
 * Класс пузырьковой сортировки массива целых чисел
 *
 * @author Kozhenenko D.D
 */
public class BubbleSort {
    public static void main(String[] args) {
        int[] massive = {6, 5, 4, 7, 3, 2, 8, 9, 1};
        printElements(massive);
        bubble(massive);
        printElements(massive);
    }

    /**
     * Обрабатывает массив целых чисел методом пузырьковой сортировки
     *
     * @param massive массив целых чисел
     */
    private static void bubble(int[] massive) {
        boolean flag = false;
        while (!flag) {
            flag = true;
            for (int i = 0; i < massive.length; i++) {
                int t;
                for (int j = i + 1; j < massive.length; j++) {
                    if (massive[j] > massive[i]) {
                        flag = false;

                        t = massive[i];
                        massive[i] = massive[j];
                        massive[j] = t;
                    }
                }
            }
        }
    }

    private static void printElements(int[] massive) {
        for (int value : massive) {
            System.out.print(value + " ");
        }
        System.out.println();

    }
}
