package insertionSort;

import java.util.Arrays;
import java.util.List;

/**
 * Класс сортировки значений ArrayList'a по убыванию методом вставки
 *
 * @author Kozhenenko D.D
 */
public class ArrayInsertionSort {
    public static void main(String[] args) {
        List<Integer> integers = Arrays.asList (1, 7, 9, 2, 4, 5, 8, 6, 3, 10);
        System.out.println (integers);
        insertionSort (integers);
        System.out.println (integers);
    }

    /**
     * Сортирует ArrayList по убыванию значений методом вставки
     *
     * @param integers ArrayList
     */
    private static void insertionSort(List<Integer> integers) {
        for (int out = 1; out < integers.size (); out++) {
            int temp = integers.get (out);
            int in = out;
            while (in > 0 && integers.get (in - 1) <= temp) {
                integers.set (in, integers.get (in - 1));
                in--;
            }
            integers.set (in, temp);
        }
    }
}



