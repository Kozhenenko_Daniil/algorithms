package insertionSort;

import java.util.Arrays;

/**
 * Класс сортировки значений массива целых чисел по возрастанию методом вставки
 *
 * @author Kozhenenko D.D
 */
public class InsertionSorting {
    public static void main(String[] args) {
        int[] array = {5, 8, 1, 4, 6, 2, 3, 7, 10, 9};
        System.out.println (Arrays.toString (array));
        insertionSort (array);
        System.out.println (Arrays.toString (array));
    }

    /**
     * Сортирует массив целых чисел по возрастанию значений методом вставки
     *
     * @param array массив целых чисел
     */
    private static void insertionSort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            int temp = array[i];
            int in = i;
            while (in > 0 && array[in - 1] >= temp) {
                array[in] = array[in - 1];
                in--;
            }
            array[in] = temp;
        }
    }
}

