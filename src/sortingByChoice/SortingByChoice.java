package sortingByChoice;

/**
 * Класс сортировки выбором массива целых чисел
 *
 * @author Kozhenenko D.D
 */

public class SortingByChoice {
    public static void main(String[] args) {
        int[] massive = {10, 3, 7, 1, 2, 8, 9, 5, 4, 6};
        printElements(massive);

        sortingByChoice(massive);

        printElements(massive);
    }

    /**
     * Выводит элементы массива
     *
     * @param massive  массив
     */
    private static void printElements(int[] massive) {
        for (int value : massive) {
            System.out.print(value + " ");
        }
        System.out.println();

    }

    /**
     * Сортирует выбором массив целых чисел
     *
     * @param massive массив для сортировки целых чисел
     */
    private static void sortingByChoice(int[] massive) {
        for (int i = 0; i < 9; i++) {
            int minI = i;
            for (int j = i + 1; j < 10; j++) {
                if (massive[j] < massive[i]) {
                    minI = j;
                }
            }
            int t = massive[i];
            massive[i] = massive[minI];
            massive[minI] = t;
        }
    }


}