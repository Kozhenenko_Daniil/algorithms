package sortingByChoice;

import java.util.Arrays;
import java.util.List;
/**
 * Класс сортировки выбором массива и ArrayList'a целых чисел
 *
 * @author Kozhenenko  D.D
 */
public class ArraySortingByChoice {
    public static void main(String[] args) {
        List<Integer> integers = Arrays.asList (5, 1, 4, 2, 9, 3, 8, 7, 5, 6);

        printElements (integers);
        sortingByChoice (integers);
        printElements (integers);
    }

    /**
     * Выводит элементы ArrayList'a
     *
     * @param integers ArrayList
     */

    private static void printElements(List<Integer> integers) {
        System.out.println (integers);
    }

    /**
     * Сортирует ArrayList целых чисел методом сортировки выбором
     *
     * @param integers ArrayList
     */
    private static void sortingByChoice(List<Integer> integers) {
        int t;
        for (int i = 0; i < integers.size () - 1; i++) {
            for (int j = i + 1; j < integers.size (); j++) {
                if (integers.get (j) < integers.get (i)) {
                    t = integers.get (i);
                    integers.set (i, integers.get (j));
                    integers.set (j, t);
                }
            }
        }
    }
}

